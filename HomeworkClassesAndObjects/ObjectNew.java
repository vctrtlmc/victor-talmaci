public class ObjectNew {

    public static void main(String[] args) {
      //Calling methods from previous class;


        Car Mercedes = new Car();
        Mercedes.init();
        Mercedes.getManufacturer("Mercedes");
        Mercedes.getEngineCapacity();
        Mercedes.yearAndModel("C200",2010);
        Mercedes.haveTurboCharger();



        System.out.println("____________________________________________");
      //New Object, new values, functions instead of methods

        Car Toyota = new Car();
        Toyota.manufacturer = "Toyota";
        Toyota.model = "Corolla";
        Toyota.haveTurboCharger = true;
        Toyota.engineCapacity = 1.4f;
        Toyota.productionYear = 2006;

        System.out.println("Manufacturer: " + Toyota.manufacturer);
        System.out.println("Model line: " + Toyota.model);
        System.out.println("Production year: " + Toyota.productionYear);
        System.out.println("Have Turbocharger: " + Toyota.haveTurboCharger);
        System.out.println("Engine capacity: " + Toyota.engineCapacity);



    }
}
