public class Variables {

    public static void main(String[] args) {

        byte by = 2;
        short sh = 100;
        int ig = 75235;
        long lg = 54454524;
        float fl = 45.66f;
        double db = 55.35;

        boolean affirmative = true;
        boolean negation = false;

        char ch = 'c';
        String st = "String Example";


        System.out.println("Byte type - " + by);
        System.out.println("Short type - " + sh);
        System.out.println("Integer type - " + ig);
        System.out.println("Long type - " + lg);
        System.out.println("Float type - " + fl);
        System.out.println("Double type - " + db);
        System.out.println("Boolean affirmative - " + affirmative);
        System.out.println("Boolean negation - " + negation);
        System.out.println("Char alphanumeric - " + ch);
        System.out.println("String alphanumeric - " + st);

    }
}
