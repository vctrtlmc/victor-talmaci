package md.tekwill;

public class OverloadedConstructors {

    public String firstName, lastName;
    public String profession;
    public int age;


    public OverloadedConstructors(String firstName, String lastName, String profession, int age) {

        this.firstName = "Tom";
        this.lastName = "Hanks";
        this.profession = "Actor";
        this.age = 63;


    }

    public OverloadedConstructors(String firstName1, String lastName2) {
        this.firstName = firstName1;
        this.lastName = lastName2;
        firstName1.charAt(0);               // Name Initials
        lastName2.charAt(0);

    }

    public OverloadedConstructors(int age, String profession) {

        this.age = 27;
        this.profession = "Programmer";

    }



}
