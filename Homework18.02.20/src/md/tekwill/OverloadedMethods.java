package md.tekwill;

public class OverloadedMethods {

    public int areaCalculator(int a, int b) {
        return a * b;

    }

    public float areaCalculator(float a, float b) {
        return a * b;

    }
    public double areaCalculator(double b, double a){
        return a*b;
    }


}
