package md.tekwill;

import java.util.Arrays;

public class Homework {

    public static int sumArrayElements(int[] a) {
        return Arrays.stream(a).sum();
    }

    public static  void evenArrayElements(int[] a) {
        for (int value : a) {
            if (value % 2 == 0) {
                System.out.println(value);

            }
        }

    }

    public static void oddArrayElement(int[] b) {
        for (int value : b) {
            if (value % 2 == 0) {
                System.out.println(value - 1);
            }
        }
    }

    public static void biArrayViewer(int[][] matrix) {

        for (int[] ints : matrix) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.printf(ints[j] + " ");
            }
            System.out.println(" ");
        }
    }

    public static void main(String[] args) {
        System.out.println(Homework.sumArrayElements(new int[]{1, 2, 3, 4, 5}));
        System.out.println("_______________");
        Homework.evenArrayElements(new int[]{1,2,3,4,5,6,7,8,9});
        System.out.println("_______________");
        Homework.oddArrayElement(new int[]{1,2,3,4,5,6,7,8,9});
        System.out.println("_______________");
        Homework.biArrayViewer(new int[][]{{5,6,4},{2,3,4}});
    }

}













