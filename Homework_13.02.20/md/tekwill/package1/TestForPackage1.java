package md.tekwill.package1;

//Access test for same package, but different access levels for classes.
public class TestForPackage1 {

    public static void main(String[] args) {


        //Object by Public Access Class

        PublicFirstClass Person = new PublicFirstClass();
        Person.name = "John";
        Person.height = 175;
        Person.weight = 70;
        Person.setNameFromPublic("John");
        System.out.println(Person.name + "'s height is: " + Person.height +", weight: "+ Person.weight);
        System.out.println("___________________________________________________");


        //Object by Default Access Class
        DefaultSecondClass Person2 = new DefaultSecondClass();
        Person2.name2 = "Bill";
        Person2.height2 = 185;
        Person2.weight2 = 75;
        Person2.showsHeightFromDefault(150);
        System.out.println(Person2.name2 + "'s height is: " + Person2.height2 +", weight: "+ Person2.weight2);
        System.out.println("___________________________________________________");

        //Object by Private Access Class
        PrivateThirdClass Person3 = new PrivateThirdClass();
        {
            System.out.println("No Variables or methods found for this person");
        }

    }
}
