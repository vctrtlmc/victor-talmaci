package md.tekwill.package2;

import md.tekwill.package1.*;

public class TestForPackage2 {
     public String name;
     protected String adress;
     private int phoneNumber;



    public static void main(String[] args) {
//Only Public variables and methods are available.
        PublicFirstClass Person = new PublicFirstClass();
        Person.name = "Paul";
        Person.height = 190;
        Person.weight = 90;
        Person.setNameFromPublic("Paul");

    }



}

class Child extends TestForPackage2{
    public static void main(String[] args) {
        TestForPackage2 User = new TestForPackage2();
        User.name = "Danny";
        User.adress = "Studentilor 9/11, Chisinau, Moldova, republic of";  // In this case we use Protected modifier
                                                                           // instead of Private. Class Child
                                                                           // can't inherit private values.
    }

}
