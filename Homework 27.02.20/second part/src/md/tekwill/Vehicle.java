package md.tekwill;

public class Vehicle {
    protected String manufacturer;
    protected String model;
    protected int productionYear;
    protected float engine;



    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public  void soundSignal() {
        System.out.println("Wroom-wroom");
    }

    public Vehicle(String manufacturer, String model, int productionYear, float engine){
        this.manufacturer = "Mercedes";
        this.model = "E Class";
        this.productionYear = 2010;
        this.engine = 2.2f;
    }

    public Vehicle(String manufacturer, String model){
        this.manufacturer = "Honda";
        this.model = "Accord";
    }

}
