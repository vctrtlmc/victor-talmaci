package md.tekwill;

public class Car extends Vehicle {


    public Car(String manufacturer, String model, int productionYear, float engine) {
        super(manufacturer, model, productionYear, engine);
        this.manufacturer = "Mazda";
        this.model = "CX 5";
        this.productionYear = 2015;
        this.engine = 2.7f;
    }


    public void soundSignal() {

        System.out.print("My fast car sounds like: ");
        super.soundSignal();

    }

    public void setManufacturer(String manufacturer) {

        this.manufacturer = manufacturer;
        System.out.println("My " + manufacturer);

    }


}



