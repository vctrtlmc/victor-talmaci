package md.tekwill.firstpart;

public interface ConfigurableMachine {
     String CpuVendor = "AMD";
     String CpuModel = "Threadripper 3990x";
     int RamCapacity = 128;
     int SsdDriveCapacity = 10000;


     void showMachineInfo();


}
