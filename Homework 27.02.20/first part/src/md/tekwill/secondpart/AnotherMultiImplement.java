package md.tekwill.secondpart;

import md.tekwill.firstpart.ConfigurableMachine;
import md.tekwill.firstpart.ConfigurableServer;

public class AnotherMultiImplement implements ConfigurableMachine, ConfigurableServer {
    @Override
    public void showServerInfo() {

    }

    @Override
    public void Server1() {

    }

    @Override
    public void showMachineInfo() {

    }
}
